extends Sprite

# Vines Background Script functions:
# - Set moviment to the background

var goal = -40000 #-40000
var speed = 0
var turn_speed = 0.02
var _max_speed = 400
var _acceleration = 0
var _ypos = -20000 #-30000 cursa curta

var player = null
var player1 = null
var flag = null

var count=0

func _ready():
	set_process(true)
	flag = get_node("../AnimatedSprite_flag")
	player = get_node("../Players/Player")
	player1 = get_node("../Players/Player1")
	player.get_node("AnimatedSprite").play("anim")
	player1.get_node("AnimatedSprite").play("anim2")

func _process(delta):
	#print(_ypos)
	if (speed>=0 and _acceleration>=0):
		if (_ypos>goal+5000): # 0 a -38000
			if (speed<=400):
				_acceleration += delta
				speed += _acceleration
			_ypos -= speed * delta
			set_region_rect(Rect2(0,_ypos,480,800))
		elif (_ypos>goal+3280): #-38000 a -39900
			get_node("../Timer_obstacles").stop()
			get_node("../Timer_bananas").stop()
			player.set_process_input(false)
			if (speed>=0):
				_acceleration -= delta/5.5
				speed -= _acceleration/4
			_ypos -= speed * delta
			set_region_rect(Rect2(0,_ypos,480,800))
			flag.play("flags")
			flag.set_pos(Vector2(flag.get_pos().x,flag.get_pos().y+delta*50))
			player.set_pos(Vector2(player.get_pos().x, player.get_pos().y-delta*40))
			player1.set_pos(Vector2(player1.get_pos().x, player1.get_pos().y-delta*40))
		else:
			player.get_node("AnimatedSprite").stop()
			player1.get_node("AnimatedSprite").stop()
			cambiar()
			count+=1
			#print("_ypos = ",_ypos," , speed = ",speed, " , acceleration = ",_acceleration, " !!!!!!!! " )

func cambiar():
	if(count==0):
		stage_manager.change_stage("res://scenes/battle.tscn")