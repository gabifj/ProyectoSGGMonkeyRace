signal listening
signal connected
signal left(id)
signal message(data)
signal start(data)
signal error(err)
signal ping(ping)

var conn

var server = {
	port = 5000,
	host = global.host,
}

var client = {
	ID = "",
	channel = 'global'
}

func _init(serverport = 5000, serverhost = global.host, serverchannel= "global", listenport = 4000):
	server.host = serverhost
	server.port = serverport
	client.channel = serverchannel
	
	conn = PacketPeerUDP.new()
	var err = conn.listen(listenport)
	if err:
		emit_signal("error", err)
	
	conn.set_send_address(server.host,server.port)
	send_data({event="connecting"})
	
func is_listening():
	if !conn.is_listening():
		return false
	
	if data_available():
		var data = get_data()
		print("data_available: ",data)
		if data.event == "connected":
			emit_signal("connected")
			client.ID = data.ID
			global.Player=data.PL
			client.channel=data.CH
			return
			
		if data.event == "left":
			emit_signal("left", data.ID)#join signal when data is received
			return
			
		if data.event == "start":
			emit_signal("start",data)
			return
		emit_signal("message",data)#message signal when data is received
		
func change_channel(channel):
	print("change_channel() "+channel)	
	client.channel = channel
	send_data({event="channel"})

func ping():
	print("ping() ")	
	send_data({event="ping", data=OS.get_ticks_msec()})

func data_available():
	#print("data_available() ")	
	if conn.get_available_packet_count() > 0:
		return true
	return false
	
func get_data():#As dictionary
	var data = conn.get_var()
	var dict = {}
	dict.parse_json(data)
	return dict
	
func broadcast_data(data): #Only accept dictionary
	print("broadcast_data(data) "+data)	
	var dat = {event="broadcast"}
	dat.data = data
	send_data(dat)
	
func multicast_data(data): #Only accept dictionary
	print("multicast_data(data) ",data)	
	var dat = {event="multicast"}
	dat.data = data
	send_data(dat)
	
func unicast_data(data, clientID): #Only accept dictionary
	print("unicast_data(data) "+data)	
	var dat = {event="unicast"}
	dat.data = data
	dat.ID = clientID
	send_data(dat)
	
func send_data(data): #Only accept dictionary
	print("send_data(data) ",data)	
	client.data = data
	conn.put_var(client.to_json())
		
func disconnect():
	print("disconnect() ")	
	send_data({event="disconnect"})
	conn.close()