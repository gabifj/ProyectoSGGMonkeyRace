extends Timer

var banana_item = preload("res://scenes/ban.tscn")

var num_banana = [ 1, 2, 3 ]
var pos_x_banana = [132, 212, 280, 353, 420]

#var ban_id = [2,2,1,3,1,3,2,1,1,1,3,2,1,3,3,2,2,1,2,1,2,1,2,3,3,1,2,3,1,2,3,3,1,2,2,3,1,1,1,3,1,1,3,2,3,3,2,2,1,1]
#var ban_pos = [0,4,3,1,4,0,1,0,0,3,3,1,3,2,3,4,2,2,0,4,2,4,3,4,0,3,0,4,4,0,0,4,1,4,3,4,4,2,4,1,1,2,1,4,3,3,3,2,3,3]

var ban_id = global.ban_id
var ban_pos = global.ban_pos
var count=0

func _ready():
	connect("timeout",self,"_on_timeout")

func _on_timeout():
	var item = banana_item.instance()
	var n = global.ban_id[count]

	item.set_pos(Vector2(pos_x_banana[global.ban_pos[count]], -400))
	get_parent().add_child(item)
	if n==2:
		var item2 = banana_item.instance()
		item2.set_pos(Vector2(pos_x_banana[global.ban_pos[count]], -365))
		get_parent().add_child(item2)
	if n==3:
		var item2 = banana_item.instance()
		item2.set_pos(Vector2(pos_x_banana[global.ban_pos[count]], -365))
		get_parent().add_child(item2)
		var item3 = banana_item.instance()
		item3.set_pos(Vector2(pos_x_banana[global.ban_pos[count]], -330))
		get_parent().add_child(item3)
	count+=1