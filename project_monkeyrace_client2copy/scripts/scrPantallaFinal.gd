extends Node2D
var music
var count=0

func _ready():
	OS.set_window_size(Vector2(800,480))
	music=get_node("music")
	music.set_volume(info.getVolume())
	music.play()
	set_process(true)
	global.conn.connect("message",self,"_on_receive")
	global.conn.connect("left",self,"_on_left")

func _process(dt):
	global.conn.is_listening()

func _on_left(data):
	print(data)

func _on_receive(data):
	print("Receive Data: ",data)
	if (data.data.has("id")):
		var monkeyname=null
		if (data.data.id=="m"):
			monkeyname="monoMarron"
		else:
			monkeyname="monoBlanco"
		var monkey = get_node(monkeyname)
		print(monkeyname)
		monkey.shoot=data.data.s
		monkey.jump=data.data.j
		monkey.move_left=data.data.l
		monkey.move_right=data.data.r
		monkey.set_pos(Vector2(data.data.x, data.data.y))
		monkey.syncro_life(data.data.v)
		monkey.numero_platanos=data.data.p
		monkey.set_ban(data.data.p)


func _exit_tree():
	print("Disconnected from server")
	global.conn.disconnect()