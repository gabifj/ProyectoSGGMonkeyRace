extends RigidBody2D


# Member variables
var anim = ""
var siding_left = true
var jumping = false
var stopping_jump = false
var shooting = false

var count_v=0
var count=0

var WALK_ACCEL = 800.0
var WALK_DEACCEL = 800.0
var WALK_MAX_VELOCITY = 200.0
var AIR_ACCEL = 200.0
var AIR_DEACCEL = 200.0
var JUMP_VELOCITY = 560.0
var STOP_JUMP_FORCE = 900.0

var MAX_FLOOR_AIRBORNE_TIME = 0.15

var airborne_time = 1e20
var shoot_time = 1e20

var MAX_SHOOT_POSE_TIME = 0.3

var move_left=false
var move_right=false
var jump=false 
var shoot=false

var monkey_enemy=null


var numero_platanos=info.get_ban_monomarron()
onready var punyo=get_node("puno")
onready var mono_platano=get_node("mono_platano")
var platanos_node
onready var vida_frame=get_node("/root/pelea_monos/vida_mono2")
onready var cara=get_node("/root/pelea_monos/cara_monos")
var timer=null
var segundos=0.3
var poder_disparar=true

var floor_h_velocity = 0.0

var platano=preload("res://scenes/platanin.tscn")

func _integrate_forces(s):
	var lv = s.get_linear_velocity()
	var step = s.get_step()
	
	var new_anim = anim
	var new_siding_left = siding_left
	
	#var spawn = Input.is_action_pressed("spawn")
	if (OS.get_name()!="Android"):
		if (global.Player=="Player"):
			move_left = Input.is_action_pressed("mono_marron_izquierda")
			move_right = Input.is_action_pressed("mono_marron_derecha")
			jump = Input.is_action_pressed("mono_marron_saltar")
			shoot = Input.is_action_pressed("mono_marron_lanzar")

	
	# Deapply prev floor velocity
	lv.x -= floor_h_velocity
	floor_h_velocity = 0.0
	
	# Find the floor (a contact with upwards facing collision normal)
	var found_floor = false
	var floor_index = -1
	if(numero_platanos==0):
		punyo.show()
		mono_platano.hide()
	for x in range(s.get_contact_count()):
		var ci = s.get_contact_local_normal(x)
		if (ci.dot(Vector2(0, -1)) > 0.6):
			found_floor = true
			floor_index = x
	
	# A good idea when impementing characters of all kinds,
	# compensates for physics imprecission, as well as human reaction delay.
	if (shoot and not shooting and poder_disparar): #and numero_platanos>0 and poder_disparar):

		poder_disparar=false
		timer.start()
		shoot_time=0
		if(numero_platanos!=0):
			numero_platanos-=1
			#print(numero_platanos)
			platanos_node.set_text(str(numero_platanos))
			#shoot_time = 0
			var bi = platano.instance()
			var ss
			if (siding_left):
				ss = -1.0
			else:
				ss = 1.0
			var pos = get_pos() + get_node("posicion_platano").get_pos()*Vector2(ss, 1.0)
		
			bi.set_pos(pos)
			get_parent().add_child(bi)
		
			bi.set_linear_velocity(Vector2(800.0*ss, -80))
			#get_node("sprite/smoke").set_emitting(true)
			#get_node("sound").play("shoot")
			PS2D.body_add_collision_exception(bi.get_rid(), get_rid()) # Make bullet and this not collide

	else:
		shoot_time += step
	#if(numero_platanos<=0):
		#print("no tengo platanos mono marron")
		#get_node("mono_platano").hide()
		#get_node("puno").show()
	if (found_floor):
		airborne_time = 0.0
	else:
		airborne_time += step # Time it spent in the air
	
	var on_floor = airborne_time < MAX_FLOOR_AIRBORNE_TIME

	# Process jump
	if (jumping):
		if (lv.y > 0):
			# Set off the jumping flag if going down
			jumping = false
		elif (not jump):
			stopping_jump = true
		
		if (stopping_jump):
			lv.y += STOP_JUMP_FORCE*step
	
	if (on_floor):
		# Process logic when character is on floor
		if (move_left and not move_right):
			if (lv.x > -WALK_MAX_VELOCITY):
				lv.x -= WALK_ACCEL*step
		elif (move_right and not move_left):
			if (lv.x < WALK_MAX_VELOCITY):
				lv.x += WALK_ACCEL*step
		else:
			var xv = abs(lv.x)
			xv -= WALK_DEACCEL*step
			if (xv < 0):
				xv = 0
			lv.x = sign(lv.x)*xv
		
		# Check jump
		if (not jumping and jump):
			lv.y = -JUMP_VELOCITY
			jumping = true
			stopping_jump = false
			#get_node("sound").play("jump")
		
		# Check siding
		if (lv.x < 0 and move_left):
			new_siding_left = true
		elif (lv.x > 0 and move_right):
			new_siding_left = false
		if (jumping):
			new_anim = "quietoConPlatano"
		elif (abs(lv.x) < 0.1):
			if (shoot_time < MAX_SHOOT_POSE_TIME):
				if(numero_platanos==0):
					new_anim="golpear"
				else:
					new_anim = "lanzarPlatano"
			else:
				new_anim = "quietoConPlatano"
		else:
			if (shoot_time < MAX_SHOOT_POSE_TIME):
				if(numero_platanos==0):
					new_anim="golpear"
				else: 
					new_anim = "lanzarPlatano"
			else:
				new_anim = "caminarConPlatano"
	else:
		# Process logic when the character is in the air
		if (move_left and not move_right):
			if (lv.x > -WALK_MAX_VELOCITY):
				lv.x -= AIR_ACCEL*step
		elif (move_right and not move_left):
			if (lv.x < WALK_MAX_VELOCITY):
				lv.x += AIR_ACCEL*step
		else:
			var xv = abs(lv.x)
			xv -= AIR_DEACCEL*step
			if (xv < 0):
				xv = 0
			lv.x = sign(lv.x)*xv
		
		if (lv.y < 0):
			if (shoot_time < MAX_SHOOT_POSE_TIME):
				if(numero_platanos==0):
					new_anim="golpear"
				else:
					new_anim = "lanzarPlatano"
			else:
				new_anim = "quietoConPlatano"
		else:
			if (shoot_time < MAX_SHOOT_POSE_TIME):
				if(numero_platanos==0):
					new_anim="golpear"
				else:
					new_anim = "lanzarPlatano"
			else:
				new_anim = "quietoConPlatano"
	
	# Update siding
	if (new_siding_left != siding_left):
		if (new_siding_left):
			get_node("cuerpo").set_scale(Vector2(0.5, 0.5))
			get_node("mono_platano").set_scale(Vector2(0.5, 0.5))
			get_node("puno").set_scale(Vector2(0.5, 0.5))
		else:
			get_node("cuerpo").set_scale(Vector2(-0.5, 0.5))
			get_node("mono_platano").set_scale(Vector2(-0.5, 0.5))
			get_node("puno").set_scale(Vector2(-0.5, 0.5))
		
		siding_left = new_siding_left
	
	# Change animation
	if (new_anim != anim):
		anim = new_anim
		get_node("anim").play(anim)
	
	shooting = shoot
	
	# Apply floor velocity
	if (found_floor):
		floor_h_velocity = s.get_contact_collider_velocity_at_pos(floor_index).x
		lv.x += floor_h_velocity
	
	# Finally, apply gravity and set back the linear velocity
	lv += s.get_total_gravity()*step
	s.set_linear_velocity(lv)

func _ready():
	info.vida_marron=20
	if (global.Player=="Player"):
		set_process(true)
		set_process_input(true)
	else:
		set_process(false)
		set_process_input(false)
	if(numero_platanos>0):
		punyo.hide()
	else:
		mono_platano.hide()
	connect("body_enter", self, "_on_body_enter")
	connect("body_enter_shape",self, "_on_monoMarron_body_enter_shape")
	timer=Timer.new()
	timer.set_one_shot(true)
	timer.set_wait_time(segundos)
	timer.connect("timeout", self, "on_timeout_complete")
	add_child(timer)
	set_ban(numero_platanos)
	monkey_enemy=get_node("/root/pelea_monos/monoBlanco")

func set_ban(num_ban):
	platanos_node = get_node("/root/pelea_monos/contador_platanos2")
	platanos_node.set_text(str(num_ban))

func on_timeout_complete():
	poder_disparar=true

func _on_body_enter(other_body):
	#print(other_body.get_name())
	if(other_body.get_name()=="platano"):
		print("me ha dado el platano")
		cara.play("cara_enfadado_marron")
		perder_vida_mono_marron(2)
		#global.conn.multicast_data({playerid="monoMarron",method="vida",type=2})
		

func _on_monoMarron_body_enter_shape( body_id, body, body_shape, local_shape ):
	#print(body_id)
	#print(body_shape)
	if(body_shape==1):
		#print("pierde vida el mono marron")
		perder_vida_mono_marron(1)
		#global.conn.multicast_data({playerid="monoMarron",method="vida",type=1})
		print() 

func perder_vida_mono_marron(vida_id):
	#print("me quitaran 1 vida")
	if(info.comprobar_vida_marron()):
		cara.play("cara_muerto_marron")
		print("Mono marron muerto. Ha ganado el mono blanco")
		stage_manager.change_stage("res://scenes/ganadorMonoBlanco.tscn")
	else:
		vida_frame.set_frame(vida_frame.get_frame()+vida_id)
		info.perder_vida_marron(vida_id)

func syncro_life(frame):
	if(info.comprobar_vida_marron()):
		count_v+=1
		if (count_v==1):
			global.conn.multicast_data({id="m",x=get_pos().x,y=get_pos().y,v=20,p=numero_platanos,s=shoot,j=jump,l=move_left,r=move_right})
			global.conn.multicast_data({id="m",x=get_pos().x,y=get_pos().y,v=20,p=numero_platanos,s=shoot,j=jump,l=move_left,r=move_right})
			cara.play("cara_muerto_marron")
			print("Mono marron muerto. Ha ganado el mono blanco")
			stage_manager.change_stage("res://scenes/ganadorMonoBlanco.tscn")
	else:
		vida_frame.set_frame(frame)
		info.vida_marron=20-frame


func _input(event):
	if(event.type==InputEvent.MOUSE_BUTTON and event.is_pressed() and not event.is_echo()):
		if(event.pos.x>monkey_enemy.get_pos().x-30 and event.pos.x<monkey_enemy.get_pos().x+30 ):
			shoot=true
			#global.conn.multicast_data({playerid="monoMarron",method="synchronise_position2",shoot=true,jump=false,move_left=false,move_right=false})
		elif(event.pos.x>get_pos().x-30 and event.pos.x<get_pos().x+30):
			jump=true
			#global.conn.multicast_data({playerid="monoMarron",method="synchronise_position2",shoot=false,jump=true,move_left=false,move_right=false})
		elif(event.pos.x < get_pos().x):
				move_left=true
				#global.conn.multicast_data({playerid="monoMarron",method="synchronise_position2",shoot=false,jump=false,move_left=true,move_right=false})
		elif(event.pos.x >get_pos().x):
				move_right=true
				#global.conn.multicast_data({playerid="monoMarron",method="synchronise_position2",shoot=false,jump=false,move_left=false,move_right=true})
	else:
		count+=1
		move_left=false
		move_right=false
		jump=false
		shoot=false

func _process(delta):
	if (count%30):
		global.conn.multicast_data({id="m",x=get_pos().x,y=get_pos().y,v=20-info.vida_marron,p=numero_platanos,s=shoot,j=jump,l=move_left,r=move_right})
