extends RigidBody2D

var vida_frame
func _ready():
	vida_frame=get_node("/root/pelea_monos/vida_mono1")
	connect("body_enter", self, "_on_body_enter")

func _on_body_enter(other_body):
	if(other_body.get_name()=="arbol_suelo"):
		print("suelo")
		queue_free()
	if(other_body.get_name()=="arbol_izquierda"):
		print("pared izquierda")
		queue_free()
	if(other_body.get_name()=="arbol_derecha"):
		print("pared derecha")
		queue_free()
	if(other_body.get_name()=="monoBlanco"):
		print("mono blanco golpeado")
		queue_free()
	if(other_body.get_name()=="platano"):
		queue_free()