extends Node2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var num
var sliderSonido
var valor
var music
func _ready():
	get_node("TextEdit").set_text(global.host)
	OS.set_window_size(Vector2(479.9,800))
	valor=get_node("valor")
	sliderSonido=get_node("nivelSonido")
	music=get_node("music")
	sliderSonido.set_value(info.getVolume())
	music.play()
	set_process(true)
func _process(delta):
	num=sliderSonido.get_value()
	valor.set_text(str(num))
	music.set_volume(num)
	info.setVolume(num)



func _on_exit_pressed():
	global.host=get_node("TextEdit").get_text()
	music.stop()
	stage_manager.change_stage("res://scenes/menuPrincipal.tscn")
