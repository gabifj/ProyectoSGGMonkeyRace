extends Area2D

var _bg = null
var _speed_factor = 1.5

func _ready():
	_bg = get_node("../Sprite_bg_vines")
	set_process(true)
	get_node("AnimatedSprite").play("anim_snake")

func _process(delta):
	set_pos(get_pos() + Vector2(0, _bg.speed * _speed_factor * delta ))
	if (get_pos().y>450):
		queue_free()

func _on_body_enter( body ):
	if "Player" in body.get_name():
		if (body.has_method("paralized")):
			body.paralized()
